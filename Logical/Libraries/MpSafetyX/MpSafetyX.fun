
FUNCTION_BLOCK MpSafetyXOptionManager (*mapp component for transfering and seting machine options on a safety controller during runtime*) (* $GROUP=mapp,$CAT=Safety,$GROUPICON=Icon_mapp.png,$CATICON=Icon_MpSafetyX.png *)
	VAR_INPUT
		MpLink : REFERENCE TO MpComIdentType; (*Connection to mapp*) (* *) (*#PAR#;*)
		Enable : BOOL; (*Enables/Disables the function block*) (* *) (*#PAR#;*)
		ErrorReset : BOOL; (*Resets  function block errors*) (* *) (*#PAR#;*)
		DataIn : UDINT; (*Input data from SafeLOGIC (input channel)*) (* *) (*#CYC#;*)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Indicates whether the function block is active*) (* *) (*#PAR#;*)
		Error : BOOL; (*Indicates that the function block is in an error state or a command was not executed correctly *) (* *) (*#PAR#;*)
		StatusID : DINT; (*Status information about the function block *) (* *) (*#PAR#; *)
		Info : MpSafetyXInfoType; (*Additional information about the component*) (* *) (*#CMD#; *)
		DataOut : UDINT; (*Output data to SafeLOGIC (output channel)*) (* *) (*#CYC#;*)
	END_VAR
	VAR
		Internal : {REDUND_UNREPLICABLE} MpComInternalDataType; (*Internal data*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK MpSafetyXOptionBoolUI (*mapp component for connecting a VC4 visualization and for setting boolean data type options on the machine*) (* $GROUP=mapp,$CAT=Safety,$GROUPICON=Icon_mapp.png,$CATICON=Icon_MpSafetyX.png *)
	VAR_INPUT
		MpLink : REFERENCE TO MpComIdentType; (*Connection to mapp*) (* *) (*#PAR#;*)
		Enable : BOOL; (*Enables/Disables the function block*) (* *) (*#PAR#;*)
		ErrorReset : BOOL; (*Resets  function block errors*) (* *) (*#PAR#;*)
		UIConnect : REFERENCE TO MpSafetyXOptionBoolUIConnectType; (*Function block parameters*) (* *) (*#CMD#; *)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Indicates whether the function block is active*) (* *) (*#PAR#;*)
		Error : BOOL; (*Indicates that the function block is in an error state or a command was not executed correctly *) (* *) (*#PAR#;*)
		StatusID : DINT; (*Status information about the function block *) (* *) (*#PAR#; *)
		Info : MpSafetyXInfoType; (*Additional information about the component*) (* *) (*#CMD#; *)
	END_VAR
	VAR
		Internal : {REDUND_UNREPLICABLE} MpComInternalDataType; (*Internal data*)
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK MpSafetyXOptionIntUI (*mapp component for connecting a VC4 visualization and for setting integer data type options on the machine*) (* $GROUP=mapp,$CAT=Safety,$GROUPICON=Icon_mapp.png,$CATICON=Icon_MpSafetyX.png *)
	VAR_INPUT
		MpLink : REFERENCE TO MpComIdentType; (*Connection to mapp*) (* *) (*#PAR#;*)
		Enable : BOOL; (*Enables/Disables the function block*) (* *) (*#PAR#;*)
		ErrorReset : BOOL; (*Resets  function block errors*) (* *) (*#PAR#;*)
		UIConnect : REFERENCE TO MpSafetyXOptionIntUIConnectType; (*Function block parameters*) (* *) (*#CMD#; *)
	END_VAR
	VAR_OUTPUT
		Active : BOOL; (*Indicates whether the function block is active*) (* *) (*#PAR#;*)
		Error : BOOL; (*Indicates that the function block is in an error state or a command was not executed correctly *) (* *) (*#PAR#;*)
		StatusID : DINT; (*Status information about the function block *) (* *) (*#PAR#; *)
		Info : MpSafetyXInfoType; (*Additional information about the component*) (* *) (*#CMD#; *)
	END_VAR
	VAR
		Internal : {REDUND_UNREPLICABLE} MpComInternalDataType; (*Internal data*)
	END_VAR
END_FUNCTION_BLOCK
