
TYPE
	MpSafetyXStatusIDType : 	STRUCT  (*MpSafetyX status ID information*)
		ID : MpSafetyXErrorEnum; (*Error code for mapp component*)
		Severity : MpComSeveritiesEnum; (*Describes the type of information supplied by the status ID (success, information, warning, error)*)
		Code : UINT; (*Code for the status ID. This error number can be used to search for additional information in the help system*)
	END_STRUCT;
	MpSafetyXDiagType : 	STRUCT  (*MpSafetyX diagnosis information*)
		StatusID : MpSafetyXStatusIDType; (*StatusID diagnostic structure *)
	END_STRUCT;
	MpSafetyXOptionUICommandType : 	STRUCT  (*Data structure for commands*)
		Command : BOOL := FALSE; (*Command triggered*)
		ButtonStatus : UINT := 2; (*Satusdatapoint for the command button*)
	END_STRUCT;
	MpSafetyXOptionUIMessageBoxType : 	STRUCT  (*Infobox data structure for MpSafetyXOption*)
		LayerStatus : UINT := 1; (*Statusdatapoint for infobox layer*)
		Type : MpSafetyXOptionUIMessageEnum := mpSAFETYXOPTION_UI_MSG_NONE; (*Type of dialog box*)
		Confirm : BOOL := FALSE; (*Error acknowledge*)
		ConfirmStatus : UINT := 1; (*Statusdatapoint for error acknowledge button*)
		StatusID : DINT; (*Code of error found during communication*)
	END_STRUCT;
	MpSafetyXOptionBoolUIDataType : 	STRUCT  (*Data for reading/writing boolean machine options*)
		SendValue : BOOL := FALSE; (*Boolean values to be set*)
		ReceivedValue : STRING[100] := ''; (*String containing the values read*)
		Name : STRING[50] := ''; (*Name of the machine option*)
		Acknowledge : BOOL := FALSE; (*Acknowledge for machine option*)
		AcknowledgeStatus : UINT := 1; (*Statusdatapoint for acknowledge*)
		Status : UINT := 1; (*Statusdatapoint for machine option *)
	END_STRUCT;
	MpSafetyXOptionBoolUIType : 	STRUCT  (*Data for reading/writing boolean machine options*)
		Option : ARRAY[0..7]OF MpSafetyXOptionBoolUIDataType; (*Machine options to be set *)
		Read : MpSafetyXOptionUICommandType; (*Data structure for sending a read request to the SafeLOGIC*)
		Write : MpSafetyXOptionUICommandType; (*Data structure for sending a write request to the SafeLOGIC*)
		Acknowledge : MpSafetyXOptionUICommandType; (*Data structure for sending an acknowledge command to the SafeLOGIC*)
		Cancel : MpSafetyXOptionUICommandType; (*Data structure for sending a cancel command to the SafeLOGIC*)
	END_STRUCT;
	MpSafetyXInfoType : 	STRUCT  (*MpSafetyX info structure*)
		Diag : MpSafetyXDiagType; (*Diagnostic structure for the function block*)
	END_STRUCT;
	MpSafetyXOptionBoolUIConnectType : 	STRUCT  (*UIConnect structure for boolean machine options*)
		Status : MpSafetyXOptionUIStatusEnum := 0; (*Status for current communication command*)
		MachineOptions : MpSafetyXOptionBoolUIType; (*Machine option data*)
		MessageBox : MpSafetyXOptionUIMessageBoxType; (*MpSafetyX infobox data structure*)
		Language : STRING[20]; (*Defines the language used to display the Machine options texts*)
	END_STRUCT;
	MpSafetyXOptionIntUIDataType : 	STRUCT  (*Data for reading/writing integer machine options*)
		SendValue : INT := 0; (*Integer values to be set*)
		ReceivedValue : STRING[100] := ''; (*String containing the values read*)
		Name : STRING[50] := ''; (*Name of the machine option*)
		Acknowledge : BOOL := FALSE; (*Acknowledge for machine option*)
		AcknowledgeStatus : UINT := 1; (*Statusdatapoint for acknowledge*)
		Status : UINT := 1; (*Statusdatapoint for machine option *)
	END_STRUCT;
	MpSafetyXOptionIntUIType : 	STRUCT  (*Data for reading/writing integer machine options*)
		Option : ARRAY[0..7]OF MpSafetyXOptionIntUIDataType; (*Machine options to be set*)
		Read : MpSafetyXOptionUICommandType; (*Data structure for sending a read request to the SafeLOGIC*)
		Write : MpSafetyXOptionUICommandType; (*Data structure for sending a write request to the SafeLOGIC*)
		Acknowledge : MpSafetyXOptionUICommandType; (*Data structure for sending an acknowledge command to the SafeLOGIC*)
		Cancel : MpSafetyXOptionUICommandType; (*Data structure for sending a cancel command to the SafeLOGIC*)
	END_STRUCT;
	MpSafetyXOptionUIStatusEnum : 
		( (*Current command state*)
		mpSAFETYXOPTION_UI_STATUS_IDLE := 0, (*UI FUB ist waiting for user interaction*)
		mpSAFETYXOPTION_UI_STATUS_RESP := 1, (*Waiting for a a response to a request*)
		mpSAFETYXOPTION_UI_STATUS_ERROR := 2 (*Communication error*)
		);
	MpSafetyXOptionUIMessageEnum : 
		( (*Message type shown in the message box*)
		mpSAFETYXOPTION_UI_MSG_NONE := 0, (*No dialog box*)
		mpSAFETYXOPTION_UI_MSG_INFO := 1, (*Dialog box: Information*)
		mpSAFETYXOPTION_UI_MSG_ERROR := 2, (*Dialog box: Errors*)
		mpSAFETYXOPTION_UI_MSG_TIMEOUT := 3 (*Dialog box: Timeout while waiting for user interaction*)
		);
	MpSafetyXOptionIntUIConnectType : 	STRUCT  (*UIConnect structure for integer machine options*)
		Status : MpSafetyXOptionUIStatusEnum := 0; (*Status for current communication command*)
		MachineOptions : MpSafetyXOptionIntUIType; (*Machine option data*)
		MessageBox : MpSafetyXOptionUIMessageBoxType; (*MpSafetyX infobox data structure*)
		Language : STRING[20]; (*Defines the language used to display the Machine options texts*)
	END_STRUCT;
END_TYPE
