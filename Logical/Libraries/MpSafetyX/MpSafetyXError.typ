
TYPE
    MpSafetyXErrorEnum : 
        ( (* Error numbers of library MpSafetyX *)
        mpSAFETYX_NO_ERROR := 0, (* No error *)
        mpSAFETYX_ERR_ACTIVATION := -1064239103, (* Could not create component [Error: 1, 0xc0910001] *)
        mpSAFETYX_ERR_MPLINK_NULL := -1064239102, (* MpLink is NULL pointer [Error: 2, 0xc0910002] *)
        mpSAFETYX_ERR_MPLINK_INVALID := -1064239101, (* MpLink connection not allowed [Error: 3, 0xc0910003] *)
        mpSAFETYX_ERR_MPLINK_CHANGED := -1064239100, (* MpLink modified [Error: 4, 0xc0910004] *)
        mpSAFETYX_ERR_MPLINK_CORRUPT := -1064239099, (* Invalid MpLink contents [Error: 5, 0xc0910005] *)
        mpSAFETYX_ERR_MPLINK_IN_USE := -1064239098, (* MpLink already in use [Error: 6, 0xc0910006] *)
        mpSAFETYX_ERR_CONFIG_INVALID := -1064239091, (* Invalid Configuration [Error: 13, 0xc091000d] *)
        mpSAFETYX_ERR_COMMUNICATION := -1064037376, (* Communication with the SafeLOGIC with ID {2:SafeLogicID} failed [Error: 5120, 0xc0941400] *)
        mpSAFETYX_ERR_TIMEOUT := -1064037375, (* A timeout has happened [Error: 5121, 0xc0941401] *)
        mpSAFETYX_ERR_ERROR_DATA := -1064037374, (* Data sent to {3:Target} and received from {2:Source} do not match [Error: 5122, 0xc0941402] *)
        mpSAFETYX_INF_WAIT_MANAGER_FB := 1083446275, (* Waiting for manager instance to become active [Informational: 5123, 0x40941403] *)
        mpSAFETYX_ERR_MISSING_UICONNECT := -1064037372 (* Missing value on UIConnect [Error: 5124, 0xc0941404] *)
        );
END_TYPE
