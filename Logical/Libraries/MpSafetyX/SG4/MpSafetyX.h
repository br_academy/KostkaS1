/* Automation Studio generated header file */
/* Do not edit ! */
/* MpSafetyX 1.62.0 */

#ifndef _MPSAFETYX_
#define _MPSAFETYX_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _MpSafetyX_VERSION
#define _MpSafetyX_VERSION 1.62.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG4
		#include "MpBase.h"
		#include "MpCom.h"
#endif

#ifdef _SG3
		#include "MpBase.h"
		#include "MpCom.h"
#endif

#ifdef _SGC
		#include "MpBase.h"
		#include "MpCom.h"
#endif

/* Datatypes and datatypes of function blocks */
typedef enum MpSafetyXOptionUIStatusEnum
{	mpSAFETYXOPTION_UI_STATUS_IDLE = 0,
	mpSAFETYXOPTION_UI_STATUS_RESP = 1,
	mpSAFETYXOPTION_UI_STATUS_ERROR = 2
} MpSafetyXOptionUIStatusEnum;

typedef enum MpSafetyXOptionUIMessageEnum
{	mpSAFETYXOPTION_UI_MSG_NONE = 0,
	mpSAFETYXOPTION_UI_MSG_INFO = 1,
	mpSAFETYXOPTION_UI_MSG_ERROR = 2,
	mpSAFETYXOPTION_UI_MSG_TIMEOUT = 3
} MpSafetyXOptionUIMessageEnum;

typedef enum MpSafetyXErrorEnum
{	mpSAFETYX_NO_ERROR = 0,
	mpSAFETYX_ERR_ACTIVATION = -1064239103,
	mpSAFETYX_ERR_MPLINK_NULL = -1064239102,
	mpSAFETYX_ERR_MPLINK_INVALID = -1064239101,
	mpSAFETYX_ERR_MPLINK_CHANGED = -1064239100,
	mpSAFETYX_ERR_MPLINK_CORRUPT = -1064239099,
	mpSAFETYX_ERR_MPLINK_IN_USE = -1064239098,
	mpSAFETYX_ERR_CONFIG_INVALID = -1064239091,
	mpSAFETYX_ERR_COMMUNICATION = -1064037376,
	mpSAFETYX_ERR_TIMEOUT = -1064037375,
	mpSAFETYX_ERR_ERROR_DATA = -1064037374,
	mpSAFETYX_INF_WAIT_MANAGER_FB = 1083446275,
	mpSAFETYX_ERR_MISSING_UICONNECT = -1064037372
} MpSafetyXErrorEnum;

typedef struct MpSafetyXStatusIDType
{	enum MpSafetyXErrorEnum ID;
	MpComSeveritiesEnum Severity;
	unsigned short Code;
} MpSafetyXStatusIDType;

typedef struct MpSafetyXDiagType
{	struct MpSafetyXStatusIDType StatusID;
} MpSafetyXDiagType;

typedef struct MpSafetyXOptionUICommandType
{	plcbit Command;
	unsigned short ButtonStatus;
} MpSafetyXOptionUICommandType;

typedef struct MpSafetyXOptionUIMessageBoxType
{	unsigned short LayerStatus;
	enum MpSafetyXOptionUIMessageEnum Type;
	plcbit Confirm;
	unsigned short ConfirmStatus;
	signed long StatusID;
} MpSafetyXOptionUIMessageBoxType;

typedef struct MpSafetyXOptionBoolUIDataType
{	plcbit SendValue;
	plcstring ReceivedValue[101];
	plcstring Name[51];
	plcbit Acknowledge;
	unsigned short AcknowledgeStatus;
	unsigned short Status;
} MpSafetyXOptionBoolUIDataType;

typedef struct MpSafetyXOptionBoolUIType
{	struct MpSafetyXOptionBoolUIDataType Option[8];
	struct MpSafetyXOptionUICommandType Read;
	struct MpSafetyXOptionUICommandType Write;
	struct MpSafetyXOptionUICommandType Acknowledge;
	struct MpSafetyXOptionUICommandType Cancel;
} MpSafetyXOptionBoolUIType;

typedef struct MpSafetyXInfoType
{	struct MpSafetyXDiagType Diag;
} MpSafetyXInfoType;

typedef struct MpSafetyXOptionBoolUIConnectType
{	enum MpSafetyXOptionUIStatusEnum Status;
	struct MpSafetyXOptionBoolUIType MachineOptions;
	struct MpSafetyXOptionUIMessageBoxType MessageBox;
	plcstring Language[21];
} MpSafetyXOptionBoolUIConnectType;

typedef struct MpSafetyXOptionIntUIDataType
{	signed short SendValue;
	plcstring ReceivedValue[101];
	plcstring Name[51];
	plcbit Acknowledge;
	unsigned short AcknowledgeStatus;
	unsigned short Status;
} MpSafetyXOptionIntUIDataType;

typedef struct MpSafetyXOptionIntUIType
{	struct MpSafetyXOptionIntUIDataType Option[8];
	struct MpSafetyXOptionUICommandType Read;
	struct MpSafetyXOptionUICommandType Write;
	struct MpSafetyXOptionUICommandType Acknowledge;
	struct MpSafetyXOptionUICommandType Cancel;
} MpSafetyXOptionIntUIType;

typedef struct MpSafetyXOptionIntUIConnectType
{	enum MpSafetyXOptionUIStatusEnum Status;
	struct MpSafetyXOptionIntUIType MachineOptions;
	struct MpSafetyXOptionUIMessageBoxType MessageBox;
	plcstring Language[21];
} MpSafetyXOptionIntUIConnectType;

typedef struct MpSafetyXOptionManager
{
	/* VAR_INPUT (analog) */
	struct MpComIdentType* MpLink;
	unsigned long DataIn;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct MpSafetyXInfoType Info;
	unsigned long DataOut;
	/* VAR (analog) */
	struct MpComInternalDataType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Error;
} MpSafetyXOptionManager_typ;

typedef struct MpSafetyXOptionBoolUI
{
	/* VAR_INPUT (analog) */
	struct MpComIdentType* MpLink;
	struct MpSafetyXOptionBoolUIConnectType* UIConnect;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct MpSafetyXInfoType Info;
	/* VAR (analog) */
	struct MpComInternalDataType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Error;
} MpSafetyXOptionBoolUI_typ;

typedef struct MpSafetyXOptionIntUI
{
	/* VAR_INPUT (analog) */
	struct MpComIdentType* MpLink;
	struct MpSafetyXOptionIntUIConnectType* UIConnect;
	/* VAR_OUTPUT (analog) */
	signed long StatusID;
	struct MpSafetyXInfoType Info;
	/* VAR (analog) */
	struct MpComInternalDataType Internal;
	/* VAR_INPUT (digital) */
	plcbit Enable;
	plcbit ErrorReset;
	/* VAR_OUTPUT (digital) */
	plcbit Active;
	plcbit Error;
} MpSafetyXOptionIntUI_typ;



/* Prototyping of functions and function blocks */
_BUR_PUBLIC void MpSafetyXOptionManager(struct MpSafetyXOptionManager* inst);
_BUR_PUBLIC void MpSafetyXOptionBoolUI(struct MpSafetyXOptionBoolUI* inst);
_BUR_PUBLIC void MpSafetyXOptionIntUI(struct MpSafetyXOptionIntUI* inst);


#ifdef __cplusplus
};
#endif
#endif /* _MPSAFETYX_ */

