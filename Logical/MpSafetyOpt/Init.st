

PROGRAM _INIT
	(* SafetyXOptionManager *)
	SafetyOptionManager.Manager.MpLink	:=	ADR(gSafetyXOptionManager);
	SafetyOptionManager.Manager.Enable := TRUE;
	
	(* SafetyXOptionINT UI *)
	SafetyOptionINT.INT_UI.Enable := TRUE;
	SafetyOptionINT.INT_UI.MpLink := ADR(gSafetyXOptionUI_0);
	SafetyOptionINT.INT_UI.UIConnect :=	ADR(SafetyOptionINT.UIConnect);
	
	(* SafetyXOptionBOOL UI *)
	SafetyOptionBOOL.BOOL_UI.Enable := TRUE;
	SafetyOptionBOOL.BOOL_UI.MpLink := ADR(gSafetyXOptionUI_1);
	SafetyOptionBOOL.BOOL_UI.UIConnect := ADR(SafetyOptionBOOL.UIConnect);
	
END_PROGRAM