
TYPE
	SafetyXOptionManager_type : 	STRUCT 
		Comm_Output : UDINT;
		Manager : MpSafetyXOptionManager;
		Comm_Input : UDINT;
	END_STRUCT;
	SafetyXOptionINT_type : 	STRUCT 
		UIConnect : MpSafetyXOptionIntUIConnectType;
		INT_UI : MpSafetyXOptionIntUI;
	END_STRUCT;
	SafetyXOptionBOOL_type : 	STRUCT 
		UIConnect : MpSafetyXOptionBoolUIConnectType;
		BOOL_UI : MpSafetyXOptionBoolUI;
	END_STRUCT;
END_TYPE
