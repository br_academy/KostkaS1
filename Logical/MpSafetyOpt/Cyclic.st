
PROGRAM _CYCLIC
	(* Insert code here *)
	SafetyOptionManager.Manager.DataIn := SafetyOptionManager.Comm_Input;
	
	SafetyOptionINT.INT_UI();
	SafetyOptionBOOL.BOOL_UI();
	SafetyOptionManager.Manager();
	
	SafetyOptionManager.Comm_Output:= SafetyOptionManager.Manager.DataOut;
	
END_PROGRAM
