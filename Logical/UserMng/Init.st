

PROGRAM _INIT
	(* Insert code here *)
	 (* User X Login *)
	UserXLogin.Login.Enable := TRUE;
	UserXLogin.Login.MpLink := ADR(gUserXLogin);	
	
	(* User X Login UI *)
	UserXLogin.LoginUI.Enable := TRUE;
	UserXLogin.LoginUI.MpLink := ADR(gUserXLogin);
	UserXLogin.LoginUI.UIConnect := ADR(UserXLogin.UIConnect);
	
	(* User X Manager UI *)
	UserXManager.ManagerUI.Enable := TRUE;
	UserXManager.ManagerUI.MpLink := ADR(gUserXLogin);
	UserXManager.ManagerUI.UIConnect := ADR(UserXManager.UIConnect);
	
PageControl.NewPage := 65535;
	
	IF (PageControl.CurrentPage <> 1) AND (UserXLogin.Login.CurrentLevel = 0) THEN
		PageControl.SetPage := 1;
	END_IF;
	
	IF (PageControl.NewPage = 65535) AND (PageControl.SetPage <> PageControl.CurrentPage) THEN
		IF (PageControl.SetPage = 0) THEN
			PageControl.SetPage := PageControl.CurrentPage;
		ELSE	
			PageControl.NewPage := PageControl.SetPage;
		END_IF
	END_IF
	
END_PROGRAM