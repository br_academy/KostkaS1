
TYPE
	MpUserXManagerUI_type : 	STRUCT 
		ManagerUI : MpUserXManagerUI;
		UIConnect : MpUserXMgrUIConnectType;
	END_STRUCT;
	PageControl_type : 	STRUCT 
		SetPage : UINT;
		NewPage : UINT;
		CurrentPage : UINT;
	END_STRUCT;
	MpUserXLogin_type : 	STRUCT 
		UIConnect : MpUserXLoginUIConnectType;
		LoginUI : MpUserXLoginUI;
		Login : MpUserXLogin;
	END_STRUCT;
END_TYPE
